# Predicting Virality of YouTube Videos
---
Authors: Adrian Hernandez and Matt Taylor

This repo corresponds to the final project for CAP-5610: Introduction to Machine Learning. 

### Structure:

* The Jupyter notebook **model.ipynb** contains the implementation of the models and the code to train and test them. 
* The notebook **kaggle-trending-dataset-exploration.ipynb** contains useful visualizations of the trending dataset.
* The relevant datasets are in the folder **data**.
    * **category_id.json :** JSON file with categories information, used to map category id to category name. 
    * **not-trending.csv :** Not trending videos dataset.
    * **not-trend-vid-categ.csv :** Table that contains the category for every video id in the not trending videos dataset.
    * **trending-videos.csv :** Dataset of over 40,000 videos that have gone trending in the US.
    * **normalized-data.csv :** This is the final dataset generated from the **not-trending.csv**,**not-trend-vid-categ.csv** and **trending-videos.csv**. This is the final dataset used to train and test the models. 
* All other notebooks were used in intermediate steps to generate/process the data. 